var width = window.innerWidth;

$(window).on("load", function(){
  // show menu
  $(".iconMenu").click(function(){
    if ($(this).hasClass("active")) {
      $(".iconMenu div").removeClass("active");
      $("#mainMenu").animate({height:0}).removeClass("showMenu");
      $(this).removeClass("active");
    } else {
      $(".iconMenu div").addClass("active");
      $("#mainMenu").addClass("showMenu");
      $(this).addClass("active");
    }
  });

  // animation blog
  $(".blog").hover(function(){
    $(".blog a").css({transform: 'translateX(0)'})
  }, function(){
    $(".blog a").css({transform: 'translateX(-40px)'})
  });
    // in view
  inView('.first h2').on('enter', function() {
    setTimeout(function(){
      $(".first h2 div span").addClass("animReveal");
    },500);
  });
  inView('.first p').on('enter', function() {
    $(".overlay .imgReveal").addClass("animWidth");
  });
  inView('.second h3').on('enter', function() {
    setTimeout(function(){
      $(".second h3 div span").addClass("animReveal");
    },500);
  });
  inView('.third h2').on('enter', function() {
    setTimeout(function(){
      $(".third h2 div span").addClass("animReveal");
    },500);
  });
  inView('.fourth h3').on('enter', function() {
    setTimeout(function(){
      $(".fourth h3 div span").addClass("animReveal");
    },500);
  });
  inView('footer h2').on('enter', function() {
    setTimeout(function(){
      $("footer h2 div span").addClass("animReveal");
    },500);
  });
});

if(width >= 768) {
 new Pageable('#container-desktop',{
    pips: false,
    animation: 800,
    orientation: 'horizontal',
    freeScroll: true
  });

  
  
    /*********************************************************/
  /* RENDERE SCROLL BEHAVIOUR VERTICAL SOLO IN PAG SERVIZI */
  /*********************************************************/

  new Pageable('.servizi #container-desktop',{
    pips: false,
    animation: 800,
    orientation: 'vertical',
    freeScroll: false
  });


} else {
  var container = document.getElementById("container-mobile");
  var heightContainer = container.style.height = "100vh";

  gsap.to(container, {
    x: () => -(container.scrollWidth - document.documentElement.clientWidth) + "px",
    ease: "none",
    scrollTrigger: {
      trigger: container,
      invalidateOnRefresh: true,
      pin: true,
      scrub: 1,
      end: () => "+=" + container.offsetWidth
    }
  });
}

inView('.home .anim-box').on('enter', function() {
  setTimeout(function(){
    $(".home .box").css({transform: 'translateY(0)'});
  },500);
});


////////////////////////////////// page federica ///////////////////////////////////////////

// in view
$(".federica footer .fb img").attr("src", "/fede2/img/fb-dark.png");
$(".federica footer .ig img").attr("src", "/fede2/img/ig-dark.png");
if(width >= 768) {
  inView('.federica footer').on('enter', function() {
    $(".federica .nav-link").css({color: '#50867f'});
    $(".federica .logo").hide();
  });
  inView('.federica footer').on('exit', function() {
    $(".federica .nav-link").css({color: '#fff'});
    $(".federica .logo").show();
  });
} else {
  inView('.federica footer').on('enter', function() {
    $(".federica .iconMenu div").css("background-color", "#085148");
    $(".federica .logo img").attr("src", "/fede2/img/logo-scuro.svg");
  });
  inView('.federica footer').on('exit', function() {
    $(".federica .iconMenu div").css("background-color", "#fff");
    $(".federica .logo img").attr("src", "/fede2/img/logo.svg");
  });
}

////////////////////////////////// page servizi ///////////////////////////////////////////

$(".servizi .iconMenu div").css("height", "4px").css("background-color", "#50867f").css("border", "1px solid #fff");

if(width >= 768) {
  inView('.servizi footer').on('enter', function() {
    $(".servizi .nav-link").css({color: '#fff'});
    $(".servizi .nav-item:nth-child(2) .nav-link").css({color: '#50867f'});
    $(".servizi .logo").hide();
  });
  inView('.servizi footer').on('exit', function() {
    $(".servizi .nav-link").css({color: '#50867f'});
    $(".servizi .nav-item:nth-child(2) .nav-link").css({color: '#085148'});
    $(".servizi .logo").show();
  });
} else {
  inView('.servizi .second').on('enter', function() {
    $(".servizi .logo img").attr("src", "/fede2/img/logo.svg");
  });
  inView('.servizi .first').on('enter', function() {
    $(".servizi .logo img").attr("src", "/fede2/img/logo-scuro.svg");
  });
  inView('.servizi footer').on('enter', function() {
    $(".servizi .iconMenu div").css("background-color", "#fff");
    $(".servizi .logo").hide();
  });
  inView('.servizi footer').on('exit', function() {
    $(".servizi .iconMenu div").css("background-color", "#50867f");
    $(".servizi .logo").show();
  });
}

////////////////////// page blog /////////////////////////////////
$(".blog-template footer .fb img").attr("src", "/fede2/img/fb-dark.png");
$(".blog-template footer .ig img").attr("src", "/fede2/img/ig-dark.png");

////////////////////////////////// page contatti ///////////////////////////////////////////

$(".contatti .iconMenu div").css("height", "4px").css("background-color", "#50867f");

if(width >= 768) {
  inView('.contatti footer').on('enter', function() {
    $(".contatti .nav-link").css({color: '#fff'});
    $(".contatti .nav-item:nth-child(4) .nav-link").css({color: '#50867f'});
    $(".contatti .logo").hide();
  });
  inView('.contatti footer').on('exit', function() {
    $(".contatti .nav-link").css({color: '#50867f'});
    $(".contatti .nav-item:nth-child(4) .nav-link").css({color: '#085148'});
    $(".contatti .logo").show();
  });
} else {
}

// back
if (width >= 768) {
  $(".back a").attr("href", "#home-1");
} else {
  $(".back a").attr("href", "#");
}
inView('.first').on('enter', function() {
  $(".back").hide();
});
inView('.first').on('exit', function() {
  $(".back").show();
});

