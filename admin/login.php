<?php 
session_start();
unset($_SESSION["username"]);

?>

<?php include "../header.html"; ?>
        <title>Servizi - Fedé - Estetica & Dedizione | Vittorio Veneto</title><!--titolo-->
    </head>
	<body class="page admin">
		<img class="logo_login m-auto d-block py-5" src="../img/logo.svg" alt="logo">
        <div class="container main p-0">
            <div class="row py-3 no-gutters">
                <div class="col-12 login">
					<h1>Login</h1>
					<form action="authenticate.php" method="post" id="form_authenticate" enctype="multipart/form-data">
						<label for="username">
							<i class="fas fa-user"></i>
						</label>
						<input type="text" name="username" placeholder="Username" id="username" required>
						<label for="password">
							<i class="fas fa-lock"></i>
						</label>
						<input type="password" name="password" placeholder="Password" id="password" required>
						<input type="submit" value="Login">
					</form>
					<div id="message"><?php if(isset($_GET['message'])){echo $_GET['message'];} ?></div>		
				</div>
			</div>
		</div>
        <!-- Script -->
        <script src="../js/jquery.min.js"></script>
        <script src="../js/main.js"></script>
	</body>
</html>
