<?php 
include "../includes/config_locale.php"; 
session_start();
?>		

<?php include "../header.html"; ?>
        <title>Servizi - Fedé - Estetica & Dedizione | Vittorio Veneto</title><!--titolo-->
    </head>
		<?php if (!isset($_SESSION['username'])){ ?>
			Non hai il permesso di accedere a questa pagina, per favore effettua il  <a href="login.php" title="Login">Login</a>
		<?php }else{ ?>
        <body class="page admin">
			<div class="mx-3 px-sm-0 pt-3">
				<p class="float-left">Benvenuto <?php echo $_SESSION['username']; ?></p>
				<p class="float-right"><a href="/" title="visita sito" target="_blank">Visita il sito</a> | <a href="login.php" title="Logout">Logout</a></p>
			</div>
			<div class="clearfix"></div>
			<div class="container-fluid">
				<div class="row py-5 no-gutters">
					<div class="col-12">
						<a class="new_post" href="crea_post.php" title="Crea post">CREA NUOVO POST</a>
						<h2>Elenco post</h2>
					</div>
				</div>
			</div>	
			<div class="container-fluid">
				<div class="row d-none d-lg-flex elenco">
					<div class="col-5">
						<p>TITOLO POST</p>
					</div>
					<div class="col-2">
						<p>IMMAGINE</p>
					</div>
					<div class="col-3">
						<p>DATA PUBBLICAZIONE</p>
					</div>
				</div>
			</div>
			
			<?php $sql = "SELECT * FROM posts ORDER BY date DESC";
					$result = $conn->query($sql); 
					while ($row = $result->fetch_assoc()) {
						$id = $row['id'];
						$title = $row['title'];
						$content = $row['content'];
						$date = $row['date'];
						$files = explode(",", $row['files']);
			?>
				<div>
					<div class="container-fluid posts">
						<div class="row align-items-center">
							<div class="col-12 col-lg-5">
								<a href="edit_post.php?id=<?php echo $id ?>"><h3 class="title_post"><?php echo $title; ?></h3></a>
							</div>
							<div class="col-12 col-lg-2">
								<img class="w-100" src="<?php echo '../upload/'. $files[0] ?>">;
							</div>
							<div class="col-12 col-lg-3">
								<p class="card-text publish_date">Data pubblicazione: <?php echo date('d.m.Y', strtotime($date)) ?></p>
							</div>
							<div class="col-12 col-lg-2 text-lg-right my-5 my-lg-0">
								<a href="edit_post.php?id=<?php echo $id ?>">Modifica | </a>
								<a href="delete_post.php?id=<?php echo $id ?>">Elimina</a>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
			<div id="message"><?php if(isset($_GET['message'])){echo $_GET['message'];} ?></div>
            <!-- Script -->
        </body>
		<?php } ?>
    </html>











