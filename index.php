<?php include "header.html"; ?>

<!-- PRIMO INDEX FILE -->
        <title>Fedé - Estetica & Dedizione | Vittorio Veneto</title><!--titolo-->
    </head>
    <body class="home">
        <?php include "menu.html"; ?>
        <div id="container">
        <!----------------------------------------------------------------------->
        <!-------------------------- Desktop block ------------------------------>
        <!----------------------------------------------------------------------->
        <div id="container-desktop">
            <div data-anchor="home 1" class="snap first">
                <div class="container-fluid">
                    <div class="logo">
                        <a href="/" title="home"><img src="/fede2/img/logo.svg" alt="logo"></a>
                    </div>      
                    <div class="row h-100 align-items-md-end justify-content-between">
                        <div class="col-md-6">
                            <h2>
                                <div><span>IL CENTRO</span></div>
                                <div><span>ESTETICO</span></div>
                                <div><span>SU MISURA</span></div>
                                <div><span>PER TE</span></div>
                            </h2>
                        </div>
                        <div class="col-md-6">
                            <div class="overlay">
                                <div class="imgReveal"></div>
                                <img src="/fede2/img/fede.jpg" alt="fede">
                            </div>
                            <p>
                                Il viaggio che stiamo per intraprendere insieme sarà emozionante e stimolante.
                            Un viaggio che parla di cambiamento, di bellezza, di corpo e di anima.
                            Ho creato un luogo che ti faccia sentire a tuo agio come a casa.
                            </p><p>Federica</p>
                            <div class="animMask">
                                <a href="#home-2"><img src="/fede2/img/freccia.png" class="arrow"><img src="/fede2/img/freccia.png" class="arrowHidden"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="home 2" class="snap second">
                <div class="container-fluid h-100">
                    <div class="row no-gutters h-100 align-items-center justify-content-between">
                        <div class="col-md-5">
                            <p>
                            FEDÉ è il nuovo centro estetico professionale a Vittorio Veneto, una nuova concezione di bellezza e benessere che passa attraverso prodotti di altissima qualità veicolati da mani esperte.
                            </p>
                            <h3>
                                <div><span class="animReveal">ESTETICA &amp;</span></div>
                                <div><span class="float-right animReveal">DEDIZIONE</span></div>
                            </h3>
                            <p>
                            Servizi di estetica avanzata al servizio delle clienti, percorsi di remise en forme studiai sulla persona, trattamenti viso personalizzati e epilazione progressiva permanente.
                            </p>
                        </div>
                        <div class="col-md-6 h-100 anim-box">
                            <div class="box primo-box">
                                <h2>ESTETICA</h2>
                                <img src="/fede2/img/box-home1.jpg">
                            </div>
                            <div class="box secondo-box">
                                <img src="/fede2/img/box-home2.jpg">
                                <h2>NATURA</h2>
                            </div>
                            <div class="box terzo-box">
                                <h2>BENESSERE</h2>
                                <img src="/fede2/img/box-home3.jpg">
                            </div>
                            <div class="box quarto-box">
                                <img src="/fede2/img/box-home4.jpg">
                                <h2>DEDIZIONE</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="home 3" class="snap third">
                <div class="container-fluid">
                    <div class="row align-items-md-end justify-content-between h-100">
                        <div class="col-md-7">
                            <h2>
                                <div><span class="animReveal">PRODOTTI</span></div>
                                <div><span class="animReveal">ED ESTETICA</span></div>
                                <div><span class="animReveal">D'AVANGUARDIA</span></div>
                                <div><span class="animReveal">PER VOI</span></div>
                            </h2>
                        </div>
                        <div class="col-md-5">
                            <h4>Trattamenti viso</h4>
                            <p>Radiofrequenza, prodotti top di gamma e massaggio con elettrostimolazione; questi sono i cardini della nostra estetica viso, declinata in base alle tue esigenze.Pelle impura, anti-age, tonificazione per citarne solo alcuni.</p>
                            <h4>Trattamento corpo</h4>
                            <p>Programmi personalizzati di remise en forme, studiati sulle tue esigenze specifiche. 
Modelliamo il corpo combinando il valore confortante del massaggio manuale con tutti i vantaggi delle ultime tecnologie, per un trattamento efficacie ma non invasivo.</p>
                            <h4>Epilazione Progressiva Permanente</h4>
                            <p>Percorsi di epilazione non invasivi e indolori, per raggiungere il risultato in sicurezza.
La nostra luce pulsata lavora per fotoemolisi selettiva, permettendo di raggiungere il follicolo pilifero attivo con precisione.</p>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="home 4" class="snap fourth">
                <div class="container-fluid h-100">
                    <div class="row align-items-center justify-content-between h-100">
                        <div class="col-md-5 col-lg-5 col-xl-5 box h-100">
                            <img src="/fede2/img/home-mix1.jpg">
                            <img src="/fede2/img/home-mix2.jpg">
                            <img src="/fede2/img/home-mix3.jpg">
                        </div>
                        <div class="col-md-7 col-lg-6 col-xl-6">
                            <p>Quando ciò che ti muove è il cuore, la fatica e le difficoltà diventano piccole, il
                                destino si muove per aprire porte inaccessibili.<br>
                                Lo sentite? Io ci metto il cuore, sempre.<br><br>
                                Venite in negozio e conosciamoci oppure cercate online i vostri prodotti preferiti.
                            </p>
                            <h3>
                                <div><span>UN CALDO</span></div>
                                <div><span class="ml-4">ABBRACCIO,</span></div>
                                <div><span class="float-right pr-5">FED&Eacute;</span></div>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="footer" class="snap">
                <?php include "footer.html"; ?>
            </div>
        </div>
            <!----------------------------------------------------------------------->
            <!-------------------------- Mobile block ------------------------------>
            <!----------------------------------------------------------------------->
        <div id="container-mobile">
            <div data-anchor="home mobile 1" class="section first">
                <div class="container-fluid">
                    <div class="logo">
                        <a href="/" title="home"><img src="/fede2/img/logo.svg" alt="logo"></a>
                    </div>
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                            <h2>
                                <div><span>IL CENTRO</span></div>
                                <div><span>ESTETICO</span></div>
                                <div><span>SU MISURA</span></div>
                                <div><span>PER TE</span></div>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="home mobile 2" class="section first">
                <div class="container-fluid">
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                            <div class="overlay">
                                <div class="imgReveal"></div>
                                <img src="/fede2/img/fede.jpg" alt="fede">
                            </div>
                            <p>
                                Un nuovo progetto sta prendendo forma, parte dal mio cuore per arrivare fino a voi.
                                Una nuova me, un nuovo percorso e un nuovo concetto di estetica professionale.
                                Un nuovo progetto sta prendendo forma, parte dal mio cuore per arrivare fino a voi.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="home mobile 3" class="section second">
                <div class="container-fluid h-100">
                    <div class="row no-gutters h-100 align-items-center">
                        <div class="col-sm-10 col-12">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eu purus vehicula,
                                consequat justo quis, sodales lectus.<br><br> Vestibulum ante ipsum primis in faucibus orci
                                luctus et ultrices posuere cubilia curae; Nullam risus nibh, egestas.
                                Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia.
                            </p>
                            <h3>
                                <div><span>ESTETICA &AMP;</span></div>
                                <div><span class="float-right">DEDIZIONE</span></div>
                            </h3>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eu purus vehicula,
                                consequat justo quis, sodales lectus.<br><br> Vestibulum ante ipsum primis in faucibus orci
                                luctus et ultrices posuere cubilia curae; Nullam risus nibh, egestas.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="home mobile 4" class="section second mobile-second">
                <div class="container-fluid h-100 p-0">
                    <div class="row no-gutters h-100 align-items-center">
                        <div class="col-12 h-100 anim-box">
                            <div class="box primo-box">
                                <h2>ESTETICA</h2>
                                <img src="/fede2/img/box-home1.jpg">
                            </div>
                            <div class="box secondo-box">
                                <img src="/fede2/img/box-home2.jpg">
                                <h2>NATURA</h2>
                            </div>
                            <div class="box terzo-box">
                                <h2>BENESSERE</h2>
                                <img src="/fede2/img/box-home3.jpg">
                            </div>
                            <div class="box quarto-box">
                                <img src="/fede2/img/box-home4.jpg">
                                <h2>DEDIZIONE</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="home mobile 5" class="section third">
                <div class="container-fluid">
                    <div class="row align-items-center h-100">
                        <div class="col-12">
                            <h2>
                                <div><span>PRODOTTI</span></div>
                                <div><span>ED ESTETICA</span></div>
                                <div><span>D'AVANGUARDIA</span></div>
                                <div><span>PER VOI</span></div>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="home mobile 6" class="section third">
                <div class="container-fluid">
                    <div class="row align-items-center h-100">
                        <div class="col-12">
                            <h4>Trattamento antiage</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut
                                lacinia consequat nisi, eget pretium sapien.
                            </p>
                            <h4>Trattamento antiage</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut
                                lacinia consequat nisi, eget pretium sapien.
                            </p>
                            <h4>Trattamento antiage</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut
                                lacinia consequat nisi, eget pretium sapien.
                            </p>
                            <h4>Trattamento antiage</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut
                                lacinia consequat nisi, eget pretium sapien.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="home mobile 7" class="section fourth">
                <div class="container-fluid h-100">
                    <div class="row align-items-center justify-content-center h-100">
                        <div class="col-12 col-sm-8 box h-100">
                            <img src="/fede2/img/home-mix1.jpg">
                            <img src="/fede2/img/home-mix2.jpg">
                            <img src="/fede2/img/home-mix3.jpg">
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="home mobile 8" class="section fourth">
                <div class="container-fluid h-100">
                    <div class="row align-items-center h-100">
                        <div class="col-sm-10 col-12">
                            <p>Quando ciò che ti muove è il cuore, la fatica e le difficoltà diventano piccole, il
                                destino si muove per aprire porte inaccessibili.<br>
                                Lo sentite? Io ci metto il cuore, sempre.<br><br>
                                Venite in negozio e conosciamoci oppure cercate online i vostri prodotti preferiti.
                            </p>
                            <h3>
                                <div><span>UN CALDO</span></div>
                                <div><span class="ml-4">ABBRACCIO,</span></div>
                                <div><span class="float-right pr-4">FED&Eacute;</span></div>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="footer mobile" class="section footer-mobile">
                <?php include "footer.html"; ?>
            </div>
            <!----------------------------------------------------------------------->
            <!----------------------------------------------------------------------->
            <!----------------------------------------------------------------------->
        </div>
        <div class="back"><a href="#home-1"><img src="img/freccia.png" alt="back"></a></div>
    </div>
        <!-- Script -->
        <script src="js/jquery-3.4.1.min.js"></script>
        <script src="js/cookiechoices.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/pageable.js"></script>
        <script src="js/in-view.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/gsap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/ScrollTrigger.min.js"></script>
        <script src="js/script.js"></script>            
    </body>
</html>
