<?php include "../includes/config_locale.php" ?>
<?php 
	if (isset($_GET['id'])) {
        $id= $_REQUEST['id'];
        $sql = "SELECT * FROM posts WHERE id=$id";
        $result = $conn->query($sql); 
	}
    while ($row = $result->fetch_assoc()) {
        $id = $row['id'];
        $title = $row['title'];
        $content = $row['content'];
        $date = $row['date'];
        $files = explode(",", $row['files']);     

?>
       <!DOCTYPE html>
        <html>
        <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="theme-color" content="#42746C"><!--color mobile head-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=yes">
    <meta name="description" content=""><!--descrizione-->
    <meta name="keywords" content=""><!--keywords-->

    <link rel="icon" href="/fede2_orig/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" type="image/x-icon" href="/fede2_orig/favicon.ico"><!--favicon-->
    <link rel="stylesheet" type="text/css" href="/fede2_orig/style/style.css" media="screen" /><!--css -->
    <link rel="stylesheet" href="/fede2_orig/style/bootstrap.css" crossorigin="anonymous"><!-- bootstrap css file  -->
    <link rel="stylesheet" type="text/css" href="../style/slick.css" />
        <link rel="stylesheet" type="text/css" href="../style/slick-theme.css" /> 
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,400;0,700;1,600&family=Oswald:wght@400;500;600;700&display=swap" rel="stylesheet">
    <!--[if lt IE 9]>
        <script src="js/respond.min.js"></script>
    <![endif]-->	
    
    <meta property="og:title" content="" /><!--titolo x fb-->
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="">
    <meta property="og:url" content="http://www." /><!--url x fb-->
    <meta property="og:image" content="http://www..jpg" /><!--img x fb-->
    <meta property="og:description" content="" /><!--descrizione x fb-->
    <meta property="og:locale" content="it_IT"><!--lingua principale del contenuto x fb-->
    <meta name="twitter:title" content="" /><!--titolo x twitter-->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:type" content="website" />
    <meta name="twitter:site" content="http://www." /><!--url x twitter-->
    <meta name="twitter:image" content="http://www..jpg" /><!--img x twitter-->
    <meta name="twitter:description" content="" /><!--descrizione per twitter-->
    <meta name="robots" content="index,follow">
    <meta name="author" content="Spring - Idee che crescono">
    <meta name="copyright" content=""><!--cliente-->
    <meta http-equiv="cache-control" content="public">
    <meta http-equiv="expires" content="86400"><!--data + 1 anno-->
    <!--Analytics-->
    </head>
    <body class="blog-template">
        <?php include "../menu.html"; ?>
            <div class="first">
                <div class="container-fluid">
                    <div class="logo">
                        <a href="/" title="home"><img src="/fede2_orig/img/logo.svg" alt="logo"></a>
                    </div>        
                </div>
                <div class="container-fluid blog-wrapper">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <h2 class="title"><?php echo $row['title'] ?></h2>

                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                        <div id="slider_news">
                                        <?php foreach ($files as $file) { ?>
                                        <div><img class="post-img" src="<?php echo '../upload/' . $file ?>"></div>
                                    <?php } ?>
                            </div>
                            <p class="testo">
                            <?php echo html_entity_decode($row['content']); ?></p>
                            <p class="date">Data di pubblicazione: <?php echo date('d/m/Y', strtotime($row['date'])) ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div>
                <?php include "../footer.html"; ?>
            </div>
        <!-- Script -->
        <script src="/fede2_orig/js/jquery-3.4.1.min.js"></script>
        <script src="/fede2_orig/js/cookiechoices.js"></script>
        <script src="/fede2_orig/js/bootstrap.min.js"></script>
        <script src="/fede2_orig/js/pageable.js"></script>
        <script src="/fede2_orig/js/in-view.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/gsap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/ScrollTrigger.min.js"></script>
        <script src="/fede2_orig/js/slick.min.js"></script>
        
        <script src="/fede2_orig/js/script.js"></script>       
        <script>// slider news
      $('#slider_news').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        pauseOnHover: false,
        pauseOnFocus: false
      });</script>     
    </body>
</html>
