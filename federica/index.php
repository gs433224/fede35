<?php include "../includes/config_locale.php" ?>
<?php include "../header.html"; ?>
        <title>Racconti di benessere - Fedé - Estetica & Dedizione | Vittorio Veneto</title><!--titolo-->
    </head>
    <body class="federica">
        <?php include "../menu.html"; ?>
        <div id="container">
        <!----------------------------------------------------------------------->
        <!-------------------------- Desktop block ------------------------------>
        <!----------------------------------------------------------------------->
        <div id="container-desktop">
            <div data-anchor="home 1" class="snap first">
                <div class="container-fluid">
                    <div class="logo">
                        <a href="/" title="home"><img src="/fede2/img/logo.svg" alt="logo"></a>
                    </div>        
                    <div class="row h-100 align-items-md-end justify-content-between">
                        <div class="col-md-6">
                            <h2>
                                <div><span>SCOPRI IL</span></div>
                                <div><span>CENTRO E</span></div>
                                <div><span>LA MIA</span></div>
                                <div><span>FILOSOFIA</span></div>
                            </h2>
                        </div>
                        <div class="col-md-6">
                            <div class="overlay">
                                <div class="imgReveal"></div>
                                <img src="/fede2/img/page-federica.jpg" alt="fede">
                            </div>
                            <p>
                                Un nuovo progetto sta prendendo forma, parte dal mio cuore per arrivare fino a voi.
                                Una nuova me, un nuovo percorso e un nuovo concetto di estetica professionale.
                                Un nuovo progetto sta prendendo forma, parte dal mio cuore per arrivare fino a voi.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="blog" class="snap second">
                <div class="container-fluid">
                    <div class="row h-100 align-items-center">
                        <?php $sql = "SELECT * FROM posts ORDER BY date DESC";
                        $result = $conn->query($sql); 
                        while ($row = $result->fetch_assoc()) {
                            $id = $row['id'];
                            $title = $row['title'];
                            $content = $row['content'];
                            $date = $row['date'];
                            $files = explode(",", $row['files']);
                        ?>
                        <div class="col-4 px-5">
                            <a href="blog.php?id=<?php echo $id; ?>" title="<?php echo html_entity_decode($title); ?>">
                            <img class="card-img-top" src="<?php echo '../upload/'. $files[0]; ?>" alt="news" />
                                <p><?php echo date('d/m/Y', strtotime($date)) ?></p>
                                <h4><?php echo html_entity_decode($title); ?></h4>
                            </a>
                        </div>
                        
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div data-anchor="blog 2" class="snap second">
                <div class="container-fluid">
                    <div class="row h-100 align-items-center">
                            <?php $sql = "SELECT * FROM posts ORDER BY date DESC";
                                $result = $conn->query($sql); 
                                while ($row = $result->fetch_assoc()) {
                                    $id = $row['id'];
                                    $title = $row['title'];
                                    $content = $row['content'];
                                    $date = $row['date'];
                                    $files = explode(",", $row['files']);
                                ?>
                            <div class="col-4 px-5">
                                <img class="card-img-top" src="<?php echo '../upload/'. $files[0]; ?>" alt="news" />
                                <p><?php echo date('d/m/Y', strtotime($date)) ?></p>
                                <h4><?php echo html_entity_decode($title); ?></h4>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div data-anchor="footer" class="snap">
                <?php include "../footer.html"; ?>
            </div>
        </div>
            <!----------------------------------------------------------------------->
            <!-------------------------- Mobile block ------------------------------>
            <!----------------------------------------------------------------------->
        <div id="container-mobile">
            <div data-anchor="federica mobile 1" class="section first">
                <div class="container-fluid">
                    <div class="logo">
                        <a href="/" title="home"><img src="/fede2/img/logo.svg" alt="logo"></a>
                    </div>        
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                            <h2>
                                <div><span>SCOPRI IL</span></div>
                                <div><span>CENTRO E</span></div>
                                <div><span>LA MIA</span></div>
                                <div><span>FILOSOFIA</span></div>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="federica mobile 2" class="section first mr-5">
                <div class="container-fluid">
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                            <div class="overlay">
                                <div class="imgReveal"></div>
                                <img src="/fede2/img/page-federica.jpg" alt="fede">
                            </div>
                            <p>
                                Un nuovo progetto sta prendendo forma, parte dal mio cuore per arrivare fino a voi.
                                Una nuova me, un nuovo percorso e un nuovo concetto di estetica professionale.
                                Un nuovo progetto sta prendendo forma, parte dal mio cuore per arrivare fino a voi.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="blog mobile" class="section second ml-5">
                <div class="container-fluid">
                    <div class="row h-100 align-items-center">
                        <div class="col-6 px-sm-4">
                            <a href="blog.php" title="">
                                <img src="/fede2/img/blog1.jpg" alt="blog">
                                <p>Data</p>
                                <h4>TITOLO DELL'ARTICOLO</h4>
                            </a>
                        </div>
                        <div class="col-6 px-sm-4">
                            <img src="/fede2/img/blog2.jpg" alt="blog">
                            <p>Data</p>
                            <h4>TITOLO DELL'ARTICOLO</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="blog mobile 2" class="section second">
                <div class="container-fluid">
                    <div class="row h-100 align-items-center">
                        <div class="col-6 px-sm-4">
                            <img src="/fede2/img/blog3.jpg" alt="blog">
                            <p>Data</p>
                            <h4>TITOLO DELL'ARTICOLO</h4>
                        </div>
                        <div class="col-6 px-sm-4">
                            <img src="/fede2/img/blog4.jpg" alt="blog">
                            <p>Data</p>
                            <h4>TITOLO DELL'ARTICOLO</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="blog mobile 3" class="section second">
                <div class="container-fluid">
                    <div class="row h-100 align-items-center">
                        <div class="col-6 px-sm-4">
                            <img src="/fede2/img/blog5.jpg" alt="blog">
                            <p>Data</p>
                            <h4>TITOLO DELL'ARTICOLO</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="footer mobile" class="section footer-mobile">
                <?php include "../footer.html"; ?>
            </div>
            <!----------------------------------------------------------------------->
            <!----------------------------------------------------------------------->
            <!----------------------------------------------------------------------->
        </div>
        <div class="back"><a href="#home-1"><img src="../img/freccia.png" alt="back"></a></div>
    </div>
        <!-- Script -->
        <script src="/fede2/js/jquery-3.4.1.min.js"></script>
        <script src="/fede2/js/cookiechoices.js"></script>
        <script src="/fede2/js/bootstrap.min.js"></script>
        <script src="/fede2/js/pageable.js"></script>
        <script src="/fede2/js/in-view.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/gsap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/ScrollTrigger.min.js"></script>
        <script src="/fede2/js/script.js"></script>            
    </body>
</html>
